import epaper
import typing

if typing.TYPE_CHECKING:
    from PIL import Image


class GDEW042T2:
    """Low-level epaper display driver"""
    
    def __init__(self):
        self._epd = epaper.epaper("epd4in2").EPD()
        self._epd.init()
        self._epd.Clear()
        
        self.height = self._epd.height
        self.width = self._epd.width
    
    def display_image(self, image: "Image") -> None:
        self._epd.display(self._epd.getbuffer(image))
        
    def clear(self) -> None:
        self._epd.Clear()
    
    def close(self) -> None:
        self.clear()
        self._epd.sleep()
        

if __name__ == "__main__":
    import time
    from PIL import Image, ImageDraw
    try:
        epd = GDEW042T2()
        Limage = Image.new("1", (epd.height, epd.width), 255)
        draw = ImageDraw.Draw(Limage)
        draw.text((2, 0), "hello world", fill=0)
        epd.display_image(Limage)
        time.sleep(2)
        epd.close()
    except (Exception, KeyboardInterrupt):
        epd.close()
