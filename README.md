# piEreader

![pie reader image](media/piEreader.jpg)
![bare pcbs](media/bare-pcbs.jpg)

## Introduction

piEreader is a proof-of-concept, open source, and DIY e-reader that supports EPUB, CBZ, PDF, and many more! **This project is still a work in progress.**

Fabrication ready files are in their appropriately named folders. The electronic design is in the `KiCad` folder.

## Schematic

### piEreader Hat

![piEreader hat](media/piEreader-hat.png)

### piEreader Daughter Board

![piEreader daughter board](media/piEreader-daugher-board.png)

## Components

- Raspberry Pi
- piEreader hat
- piEreader daughter board
- piEreader mounting plate
- Double sided tape
- (5x) [6mm Tactile switches](https://www.adafruit.com/product/367)
- (2x) [1x7 PicoBlade connectors](https://www.molex.com/molex/products/part-detail/pcb_headers/0530480710)
- [1x7 PicoBlade cable](https://www.adafruit.com/product/4927)
- [2x20 stack-able headers](https://www.adafruit.com/product/1979)
- [GDEW042T2 4.2" e-Paper display](https://www.waveshare.com/4.2inch-e-paper.htm)
- [Waveshare Universal e-Paper Hat](https://www.waveshare.com/e-Paper-Driver-HAT.htm)
- (12x) [M2.5 heat-set threaded inserts](https://www.mcmaster.com/92000A201)
- (12x) [M2.5 x 0.45 mm screws](https://www.mcmaster.com/94180A321)

You'll need a soldering iron to install the M2.5 heat-set threaded inserts. If you don't have one you can just tape everything.

Also, I recommend at least using a Raspberry Pi 3 as anything less powerful is pretty slow.

## Build Instructions

*READ ALL INSTRUCTIONS BEFORE STARTING WORK.* It is possible to install components incorrectly.

The piEreader mounting plate can be 3D printed or laser cut. Check the STL and DXF folders for the appropriate files.

I recommend marking the top and back of the mounting plate before installing the threaded inserts. Below are labeled photos.

![top of mounting plate](media/top_mounting_plate.JPG)

![back of mounting plate](media/back_mounting_plate.JPG)

Use a soldering iron to install all the heat-set threaded inserts. Or skip this step if you're lazy like me and taped everything.

Solder 5 tactile switches and one 1x7 PicoBlade connector onto the piEreader daughter board.

![picture of daughter board](media/assembled-piEreader-daughter-board.jpg)

Solder 2x20 headers and a 1x7 PicoBlade connector onto the piEreader hat.

![picture of piEreader hat](media/assembled-piEreader-hat.jpg)

Before installing any components on the mounting plate, I recommend connecting all the ribbon cables of the e-Paper display, Waveshare Universal e-Paper hat, and ribbon cable extender.

Install the Raspberry Pi onto the back of the mounting plate. Then install the soldered piEreader hat onto the Raspberry Pi. Connect the 1x7 PicoBlade cable into the piEreader hat.

*NOTE: The cable between the piEreader hat and the piEreader daughter board can only be installed when the Waveshare Universal e-Paper hat is not installed.*

![Photo of installed piEreader hat](media/installed-piEreader-hat.jpg)

Install the Waveshare Universal e-Paper hat on top of the piEreader hat.

![Photo of installed Waveshare hat](media/installed-waveshare-hat.jpg)

Install the ribbon cable extender.

![Photo of installed ribbon cable extender](media/installed-ribbon-cable-extender.jpg)

Flip the mounting plate over. Use double sided tape to install the e-Paper display. Install the piEreader daughter board, then connect it to the 1x7 PicoBlade connector. You're done!

![pie reader image](media/piEreader.jpg)
