import RPi.GPIO as GPIO


class ButtonMonitor:
    """Listen for button presses."""

    def __init__(self):        
        self._up_pin = 27
        self._down_pin = 22
        self._right_pin = 23
        self._left_pin = 5
        self._select_pin = 6
        self._buttons = [self._up_pin, self._down_pin, self._right_pin, self._left_pin, self._select_pin]
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self._buttons, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

        for button in self._buttons:
            GPIO.add_event_detect(button, GPIO.FALLING)

    def up_pressed(self) -> bool:
        """If up was pressed."""
        return GPIO.event_detected(self._up_pin)

    def right_pressed(self) -> bool:
        """If right was pressed."""
        return GPIO.event_detected(self._right_pin)

    def down_pressed(self) -> bool:
        """If down was pressed."""
        return GPIO.event_detected(self._down_pin)

    def left_pressed(self) -> bool:
        """If left was pressed."""
        return GPIO.event_detected(self._left_pin)

    def select_pressed(self) -> bool:
        """If select was pressed."""
        return GPIO.event_detected(self._select_pin)

    def close(self) -> None:
        """Clean up gpios."""
        for button in self._buttons:
            GPIO.remove_event_detect(button)


if __name__ == "__main__":
    monitor = ButtonMonitor()
    try:
        while 1:
            if monitor.up_pressed():
                print("up.")
            elif monitor.right_pressed():
                print("right.")
            elif monitor.left_pressed():
                print("left.")
            elif monitor.down_pressed():
                print("down.")
            elif monitor.select_pressed():
                print("select.")
        monitor.close()
    except (Exception, KeyboardInterrupt):
        monitor.close()
