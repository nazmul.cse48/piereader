import logging
import os
from pathlib import Path
from PIL import Image, ImageDraw

from fonts import FontMaker


class BookPicker:
    """Book selection UI."""

    def __init__(self):
        self._books_dir = str(Path.home()) + "/Bookshelf/"
        self._books = os.listdir(self._books_dir)
        self._selected_book_path = ""
        self._book_idx = 0  # lowest index is at top of screen
        self._book_index_changed = False

    def make_ui(self, size: tuple[int, int]) -> Image:
        """Build displayable UI image."""
        height, width = size
        image = Image.new("1", size, 255)

        fontmaker = FontMaker()
        draw = ImageDraw.Draw(image)
        draw.text((width / 8, 0), "Select a Book", font=fontmaker.font(size=24), fill=0)
        height_factor = 0.3
        for idx, book_name in enumerate(self._books):
            y = height * height_factor
            draw.text((2, y), book_name, font=fontmaker.font(size=14), fill=0)
            if self._book_idx == idx:
                draw.rectangle((0, y, 1, y + 15), fill = 0)
                self._selected_book_path = self._books_dir + book_name
                self._book_index_changed = False
            height_factor += 0.1
        return image

    def bookpath(self) -> str:
        """Absolute path to selected book."""
        return self._selected_book_path

    def book_index_changed(self) -> bool:
        """Acessor to book index changed flag."""
        return self._book_index_changed

    def increment_book_index(self) -> None:
        """Increase book selection index."""
        index = self._book_idx + 1
        if index > len(self._books) - 1:
            logging.info(f"Did not increase selection index to {index}")
        else:
            self._book_idx = index
            self._book_index_changed = True

    def decrement_book_index(self) -> None:
        """Decrease book selection index."""
        index = self._book_idx - 1
        if index < 0:
            logging.info(f"Did not decrease selection index to {index}")
        else:
            self._book_idx = index
            self._book_index_changed = True


if __name__ == "__main__":
    import time
    from display import Display

    disp = Display()
    picker = BookPicker()
    try:
        disp.display_image(picker.make_ui(disp.size()))
        time.sleep(2)
        disp.close()
    except (Exception, KeyboardInterrupt):
        disp.close()
